# delightful CLI [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of delightful CLI software for console lovers, and of course all FOSS.

Everyone is invited to contribute. To do so, please read our [guidelines](https://codeberg.org/teaserbot-labs/delightful/).

## Table of contents

- [Audio players](#audio-players)
  - [MPD clients](#mpd-clients)
- [BitTorrent](#bittorrent)
- [Download managers](#download-managers)
- [Email](#email)
- [File managers](#file-managers)
- [Instant messaging](#instant-messaging)
  - [IRC](#irc)
  - [Matrix](#matrix)
  - [Multi-protocol](#multi-protocol)
  - [Tox](#tox)
  - [XMPP](#xmpp)
- [Microblogging](#microblogging)
- [Password Managers](#password-managers)
- [Podcast](#podcast)
- [RSS](#rss)
- [Text editors](#text-editors)
- [Office tools](#office-tools)
- [Video players](#video-players)
- [(Non-)Web browsers](#non-web-browsers)
- [Miscellaneous](#miscellaneous)
- [Maintainers](#maintainers)
- [License](#license)


## Audio players
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [cmus](https://cmus.github.io/) | cmus is a small, fast and powerful console music player for Unix-like operating systems | C |  GPLv2 |
| [mpg123](https://www.mpg123.org/) | Fast console MPEG Audio Player and decoder library | C, Assembly | LGPL2.1 |
| [pianobar](https://6xq.net/pianobar/) | pianobar is a free/open-source, console-based client for the personalized online radio Pandora | C | MIT |

### MPD clients
[MPD](https://www.musicpd.org/) stands for **M**usic **P**layer **D**aemon and it is one of the most extensible and powerful audio tools available on Linux. It acts as a "server" and you interact with it through a client, of which there are many, graphical, web-based, as well as CLI. For a broader list, see the [Arch Wiki page](https://wiki.archlinux.org/index.php/Music_Player_Daemon#Command-line)

| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [clerk](https://github.com/carnager/clerk) | MPD client using rofi or fzf | Perl | MIT |
| [fmui](https://github.com/seebye/fmui) | **f**zf **m**pd **u**ser **i**nterface | Shell | GPL-3.0 |
| [mpc](https://www.musicpd.org/doc/mpc/html/) | `mpc` connects to a MPD and controls it according to commands and arguments passed to it | C | GPL-2.0 |
| [ncmpcpp](https://rybczak.net/ncmpcpp/) | featureful ncurses based MPD client inspired by ncmpc | C++ | GPL-2.0 |


## BitTorrent
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [Deluge](https://deluge-torrent.org/) | is a lightweight, Free Software, cross-platform BitTorrent client | Python | GPLv3 |
| [rTorrent](https://rakshasa.github.io/rtorrent/) | Simple and lightweight ncurses BitTorrent client. Requires libtorrent backend | C++ | GPLv2 |
| [Transmission-CLI](https://transmissionbt.com/) | A Fast, Easy and Free BitTorrent Client | C, Objective-C | GPLv2 or GPLv3 |


## Download managers
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [aria2](https://aria2.github.io/) | aria2 is a lightweight multi-protocol & multi-source command-line download utility | C++ | GPLv2 |
| [lftp](https://github.com/lavv17/lftp) | Sophisticated command line file transfer program (ftp, http, sftp, fish, torrent) | C++ | GPLv3 |
| [You-Get](https://you-get.org/) | Tiny command-line utility to download media contents (videos, audios, images) from the Web, in case there is no other handy way to do it | Python | MIT |
| [youtube-dl](https://ytdl-org.github.io/youtube-dl/index.html) | A command-line program to download videos from YouTube.com and a few more sites | Python |  Unlicense |
| [wget](https://www.gnu.org/software/wget/) | Is a free software package for retrieving files using HTTP, HTTPS, FTP and FTPS the most widely-used Internet protocols | C | GPLv3 |

## Email
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [aerc](https://aerc-mail.org/) | aerc is an email client that runs in your terminal | Go | MIT |
| [alot](https://github.com/pazz/alot) | Terminal-based Mail User Agent | Python | GPLv3 |
| [Alpine](http://alpine.x10host.com/alpine/release/) | Fast, easy-to-use and Apache-licensed email client based on Pine | C | Apache |
| [Himalaya](https://github.com/soywod/himalaya) | A CLI email client written in Rust. The aim of Himalaya is to extract the email logic into a simple (yet solid) CLI API that can be used directly from the terminal, from scripts, from UIs… Possibilities are endless! | Rust, Nix, Lua, Shell | BSD-3-Clause |
| [mu/mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) | Maildir indexer/searcher + emacs mail client + guile bindings |  | GPLv3 |
| [Mutt](http://mutt.org) |Small but very powerful text-based mail client| C | GPLv2+ |
| [NeoMutt](https://neomutt.org/) | Command line mail reader (or MUA). It's a fork of Mutt with added features | C | GPLv2 |
| [notmuchmail](https://notmuchmail.org) | A fast mail indexer built on top of xapian | C | GPLv3 |
| [S-nail](https://www.sdaoden.eu/code.html#s-mailx) | S-nail provides a simple and friendly environment for sending and receiving mail | C | BSD |

## File managers
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [fff](https://github.com/dylanaraps/fff) | A simple file manager written in bash | Bash |  MIT |
| [lf](https://github.com/gokcehan/lf) | Terminal file manager written in Go using server/client architecture | Go | MIT |
| [Midnight Commander](https://midnight-commander.org/) | Console-based, dual-paneled file manager | C | GPLv2 |
| [n³/nnn](https://github.com/jarun/nnn) | Tiny, lightning fast, feature-packed file manager | C | BSD-2-Clause |
| [ranger](https://ranger.github.io/) | A VIM-inspired filemanager for the console | Python | GPLv3 |
| [Vifm](https://vifm.info/) | Vifm is a file manager with curses interface, which provides Vi[m]-like environment for managing objects within file systems, extended with some useful ideas from mutt | C | GPLv2 |


## Instant messaging

### IRC
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [BitchX](http://bitchx.sourceforge.net/) | A full-featured, scriptable free software IRC client for UNIX-like systems | C | GPLv2 |
| [catgirl](https://git.causal.agency/catgirl/about/) | is a TLS-only terminal IRC client. | C | GPLv3 |
| [ii](https://tools.suckless.org/ii/) | is a minimalist FIFO and filesystem-based IRC client | C |  MIT/X |
| [Irssi](https://irssi.org/) | Your text mode chatting application since 1999. IRC built-in. Multi-protocol friendly for module authors | C | GPLv2 |
| [tiny](https://github.com/osa1/tiny) | an IRC client written in Rust with a clutter-free interface | Rust |  MIT |
| [WeeChat](https://weechat.org/) | Modular, lightweight ncurses-based IRC client | C | GPLv3 |

### Matrix
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [gomuks](https://maunium.net/go/gomuks) |A terminal based Matrix client written in Go | Go | AGPLv3 |
| [weechat-matrix](https://github.com/poljar/weechat-matrix) | Weechat Matrix protocol script written in python | Python | MIT |

### Multi-protocol
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [BitlBee](https://www.bitlbee.org/) | BitlBee brings IM (instant messaging) to IRC clients | C | GPLv2 |
| [Finch](https://developer.pidgin.im/wiki/Using%20Finch) | Pidgin but CLI | C | GPLv2+ |

### Tox
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [Toxic](https://github.com/Jfreegman/toxic) | An ncurses-based Tox client | C | GPLv3 |

### XMPP
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [Profanity](https://profanity-im.github.io/) | Profanity is a console based XMPP client written in C using ncurses and libstrophe, inspired by Irssi | C | GPLv3+ |

## Microblogging
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [Rainbow Stream](https://github.com/orakaro/rainbowstream) | A smart and nice Twitter client on terminal written in Python | Python | MIT |
| [toot](https://github.com/ihabunek/toot) | Mastodon CLI & TUI client | Python | GPLv3 |
| [turses](https://github.com/louipc/turses) | A twitter client for the console | Python | GPLv3 |
| [tut](https://github.com/RasmusLindroth/tut) | a Mastodon TUI | Go | MIT |

## Password Managers
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [gopass](https://www.gopass.pw/) | gopass is a simple but powerful password manager for your terminal | Go | MIT |
| [KeePassXC](https://keepassxc.org/) | keepassxc-cli for the terminal, but installs a GUI | C++ | Multiple |
| [lesspass](https://lesspass.com) | LessPass is a stateless password manager | Python | GPLv3 |
| [pass](https://www.passwordstore.org/) | the standard unix password manager | Bash | GPLv2+ |

## Podcast
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [castero](https://github.com/xgi/castero) | TUI podcast client for the terminal | Python | MIT |
| [castget](https://castget.johndal.com/) | A command-line podcast downloader | C | GPLv2 |
| [marrie](https://github.com/rafaelmartins/marrie/) | A simple podcast client that runs on the CLI | Python | BSD-3-Clause |
| [pcd](https://github.com/kvannotten/pcd) | CLI podcast client (podcatcher) written in golang | Go | GPLv3 |
| [shellcaster](https://github.com/jeff-hughes/shellcaster) | Terminal-based podcast manager built in Rust | Rust | GPLv3 |

## RSS
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [Canto](https://codezen.org/canto-ng/) | Canto is an Atom/RSS feed reader for the console that is meant to be quick, concise, and colorful | Python | GPLv2 |
| [Newsboat](https://newsboat.org/) | Newsboat is an RSS/Atom feed reader for the text console. It’s an actively maintained fork of Newsbeuter | C++, Rust | MIT |
| [Snownews](https://github.com/kouya/snownews) | Snownews is a command-line RSS feed reader. It runs on everything Unix | C, Perl | GPLv3 |
| [Sfeed](https://codemadness.org/sfeed-simple-feed-parser.html) | Sfeed is a RSS and Atom parser (and some format programs). It converts RSS or Atom feeds from XML to a TAB-separated file | C | ISC |


## Text editors
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [dte](https://craigbarnes.gitlab.io/dte/) | A small and easy to use console text editor | C | GPLv2 |
| [Emacs](https://www.gnu.org/software/emacs/emacs.html) | The extensible, customizable, self-documenting real-time display editor by GNU | Lisp, C | GPLv3 |
| [Helix](https://github.com/helix-editor/helix) | A kakoune / neovim inspired editor, written in Rust. | Rust | MPL-2.0 |
| [Howl](https://howl.io/) | A general purpose, fast and lightweight editor with a keyboard-centric minimalistic user interface | Moonscript/Lua | MIT |
| [Kakoune](https://kakoune.org/) | Modal editor · Faster as in less keystrokes · Multiple selections · Orthogonal design | C++ | Unlicense |
| [micro](https://micro-editor.github.io/) | a modern and intuitive terminal-based text editor | Go | MIT |
| [nano](https://nano-editor.org/) | Console text editor based on pico with on-screen key bindings help | C | GPLv2 |
| [Neovim](https://neovim.io/) | Vim's rebirth for the 21st century | Vim script, C, Lua | Apache |
| [tilde](https://os.ghalkes.nl/tilde/) | Tilde is a text editor for the console/terminal, which provides an intuitive interface for people accustomed to GUI environments such as Gnome, KDE and Windows | C++ | GPLv3 |
| [vim](https://www.vim.org/) | Vim is a highly configurable text editor built to make creating and changing any kind of text very efficient. | C, Vim Script | VIM License |
| [Vis](https://github.com/martanne/vis) | A vi-like editor based on Plan 9's structural regular expressions | C, Lua | ISC |

## Office tools
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [present](https://github.com/vinayak-mehta/present) | A terminal-based presentation tool with colors and effects. | Python | Apache 2.0 |
| [slides](https://github.com/maaslalani/slides) | A terminal-based presentation tool. | Go | MIT |
| [Taskwarrior](https://github.com/GothenburgBitFactory/taskwarrior) | A GTD, todo list, task management, command line utility with a multitude of features. | C++, Perl, Python | MIT |

## Video players
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [MPlayer](https://mplayerhq.hu/design7/news.html) | Video player that supports a complete and versatile array of video and audio formats | C | GPLv2 |
| [mpv](https://mpv.io/) | Movie and audio player based on MPlayer and mplayer2 | C, Objective-C, Lua | GPLv2 |
| [VLC](https://www.videolan.org/vlc/) | Command-line version of the famous video player that can play smoothly high definition videos in the TTY. The rc interface can be launched with vlc -I rc, and the ncurses interface can be launched with vlc -I ncurses | C | LGPG2.1 & GPLv2 |


## (Non-)Web browsers
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [amfora](https://github.com/makeworld-the-better-one/amfora) | A fancy terminal browser for the Gemini protocol | Go | GPLv3 |
| [astronaut](https://git.sr.ht/~adnano/astronaut/) | A Gemini browser for the terminal | Go | GPLv3 |
| [asuka](https://git.sr.ht/~julienxx/asuka) | asuka is a Gemini Project client written in Rust with NCurses | Rust | MIT |
| [AV-98](https://tildegit.org/solderpunk/AV-98) | A CLI-based Gemini Client | Python | BSD 2-clause |
| [bolluks](https://git.sr.ht/~acdw/bollux) | a bash Gemini client | Bash | MIT |
| [Bombadillo](https://tildegit.org/sloum/Bombadillo) | Bombadillo is a non-web client that supports Gopher, Gemini and many more | Go | GPLv3 |
| [Browsh](https://github.com/browsh-org/browsh) | A fully-modern text-based browser, rendering to TTY and browsers | Javascript, Go | LGPL-2.1 |
| [Elpher](https://thelambdalab.xyz/elpher/) | A Gopher/Gemini client for GNU Emacs | Lisp | GPLv3 |
| [Lynx](https://lynx.invisible-island.net/lynx.html) | Text browser that supports http(s) and Gopher | ISO C | GPLv2 |

## Miscellaneous
| Website | Summary | Written in | License |
| :---: | --- | :---: | :---: |
| [asciinema](https://asciinema.org/) | Easily record terminal sessions and replay them in a terminal as well as in a web browser. | Python | GPLv3 |
| [dijo](https://github.com/NerdyPepper/dijo) | A scriptable habit tracker that runs in your terminal. | Rust | MIT |
| [duf](https://github.com/muesli/duf) | Disk Usage/Free Utility - a better 'df' alternative | Go | MIT |
| [Glow](https://github.com/charmbracelet/glow) | A terminal based markdown reader designed from the ground up to bring out the beauty—and power—of the CLI | Go | MIT |
| [ocm](https://git.sr.ht/~shellowl/ocm/) | Owl's Contact Manager - The pass-like contact manager | Shell | GPLv3 |
| [vdx](https://github.com/yuanqing/vdx) | An intuitive CLI for processing video, powered by FFmpeg | Typescript | MIT |


## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/chaihron/delightful-cli/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@chaihron`](https://codeberg.org/chaihron)

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](http://i.creativecommons.org/p/mark/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)
