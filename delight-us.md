# Creating a delightful list

So you are considering adding more delight to this project? That is wonderful! Here's some easy instructions to set up your own curated [delightful](delightful.md) list.

## On this page

- [Overall structure](#user-content-overall-structure)
- [The README.md file](#user-content-the-readme-md-file)
  - [Heading](#user-content-heading)
  - [Summary](#user-content-summary)
  - [Badge](#user-content-badge)
  - [Contents](#user-content-contents)
  - [Category layout](#user-content-category-layout)
  - [Table layout](#user-content-table-layout)
  - [Mixed layout](#user-content-mixed-layout)
  - [Maintainers](#user-content-maintainers)
  - [License](#user-content-license)
- [Attribution of contributors](#user-content-attribution-of-contributors)

## Overall structure

Your repository should have the following structure:

```
<repository root>
  |
  |-- README.md
  |-- delightful.md
  |-- delight-us.md
  |-- delightful-contributors.md
  |-- LICENSE
  └-- [assets]
        |
        └-- my-delightful-logo.png
```

Explanation of the files:

- `README.md` - the delightful list itself.
- `delightful.md` - exact copy of [delightful](delightful.md) explainer in this project ([raw text](https://codeberg.org/teaserbot-labs/delightful/raw/branch/main/delightful.md)).
- `delight-us.md` - explanation on how to contribute to your list (borrow texts from this project).
- `delightful-contributors.md` - optional list of contributors who added list entries.
- `LICENSE` - Public domain [CC0](https://creativecommons.org/publicdomain/zero/1.0/) license text. Copy from this project ([raw text](https://codeberg.org/teaserbot-labs/delightful/raw/branch/main/LICENSE)).
- `assets` folder - place all images here, if you have more than just the list logo.

## The README.md file

### Heading

The heading of the list can be one of two options:

1. Regular markdown heading (single `#`), starting with 'delightful' in small caps, followed by the list topic.
2. List logo image displaying the name of the list, and preferably the text 'delightful' too (not required).

If you choose a logo image - which is the preferred option - then you should center it:

```html
<div align="center">
  <img src="https://link_to/delightful-<list-topic>/assets/my-delightful-logo.png" alt="delightful-<list-topic>"/>
</div>
```

### Summary

Provide a brief, clear summary on why the list was create and what is collected on it. End the summary with a line telling people how they can contribute, and include a link to your `delight-us.md` page for more instructions. For example:

```markdown
<summary-text>

Together we collected these gems of freedom :gem: Please [delight us](delight-us.md) and add your own.
```

### Badge

The &nbsp;[![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)&nbsp; badge is required and either added after the markdown title, or somewhere in the summary block. See the [checklist](delight-checks.md#user-content-delightful-badge) for more info.

### Contents

The table of contents section must be simply named 'Contents' and is a heading 2 (i.e. `## Contents`). Inclusion is required if you choose for a [category layout](#user-content-category-layout), and optional otherwise. But you should include it if your [table layout](#user-content-table-layout) becomes too long and unwieldy.

For category- and mixed layout the format is:

```markdown
## Contents

- [Category1](#category1-anchor)
  - [Subcategory1-1](#subcategory1-1-anchor)
  - [Subcategory1-2](#subcategory1-2-anchor)
- [Category2](#category2-anchor)

... etcetera

- [Maintainers](#maintainers)
- [License](#license)
```

The [Maintainers](#user-content-maintainers) and [License](#user-content-license) section must be part of the table of contents. You can separate them from the main entries (see our [table of contents](README.md#user-content-contents)).

For table layout the format is:

```markdown
## Contents

### [A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)
```

Which gives:

> ## Contents
>
>## [A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

**Note**: Test your anchors to see if they work. Some software, such as Gitea require an anchor prefix to be added (in Gitea's case this is `#user-content-`).

### Category layout

In this list layout the body of the list is divided into separate Category sections. There can be two levels: main categories and subcategories, and no deeper nesting. The entries start with a link to the delightful resource, followed by a short, concise description, separated by a dash from the link.

For the description holds: the shorter the better, preferably fitting a single line. Example in markdown:

```markdown
## Category name

<optional-category-explainer>

- [Delightful resource1](https://link_to/resource1) - This is a delightful short description.

### Subcategory name

<optional-category-explainer>

- [Delightful resource2](https://link_to/resource2) - This is a delightful short description too.

... etcetera
```

If it helps clarify the list, a short (max. 2-3 lines) category explainer text can be added. With good category names this should be rarely needed.

### Table layout

In table layout you either have a single, alphabetically sorted table, or - for long lists - you have a glossary layout with headings (`##`) for each character in the alphabet, followed with a table holding resources starting with that character.


You can choose your own column layout and column header labels, as long as:

- The first column only holds the link to the delightful resource.
- One other column holds the description of the resource.

An example of table layout with glossary sections:

```markdown
## A

| Project | Summary | License |
| --- | --- | --- |
| [Anton](https://link_to/anton-project) | Anton project description | `AGPLv3` |

## B

| Project | Summary | License |
| --- | --- | --- |
| [Bernard](https://link_to/bernard-project) | Bernard project description | `MIT` |

## C

...etcetera
```

### Mixed layout

You can also choose a combination of category layout and table layout, where you have categories followed by tables that hold the resource links. In the case of mixed layout a [table of contents](#user-content-contents) section is required.

### Maintainers

Your list must explicitly mention who maintains it, by adding the following text in the Maintainers section, just below the actual list content (near the end of your README.md):

```markdown
If you have questions or feedback regarding this list, then please create
an [Issue](https://link_to.issue-tracker) in our tracker, and optionally
`@mention` one or more of our maintainers:

- [`@<placeholder-maintainer1>`](https://link_to.maintainer1-userprofile)
- [`@<placeholder-maintainer2>`](https://link_to.maintainer2-userprofile)
```

This is the minimum notation for maintainers, but a full reference can include (in that same order, on a single line):

- Full name of maintainer (can be a link to personal website or blog).
- User account name + link to user profile of the maintainer.
- One more location where maintainer can be reached (in parentheses).

Example:

> ## Maintainers
>
> If you have questions or feedback regarding this list, then please create an [Issue](https://link_to.issue-tracker) in our tracker, and optionally
`@mention` one or more of our maintainers:
>
> - [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))

### License

The final part of the README.md is the licens section which must be public domain [CC0 Public domain](https://creativecommons.org/publicdomain/zero/1.0/). At minimum this shows the proper logo with a link to the license text, like so:

``` markdown
[![CC0 Public domain. This work is free of known copyright restrictions.](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)
```

You can also add an explicit waiver of copyright, if you wish:

```
To the extent possible under law, the [maintainers](#maintainers) and other [contributors](delightful-contributors.md) have waived all copyright and related or neighboring rights to this work.
```

## Attribution of Contributors

You can optionally decide to attribute your contributors by adding a `delightful-contributors.md` file to your repository (see the [Contributors](delightful-contributors.md) file in this project). The format for Contributor entries is the same as the one described above for [Maintainers](#user-content-maintainers).

> **Note**: The contributors list is _not_ alphabetically sorted, so please add yourself at the bottom of the list.

